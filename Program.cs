﻿using System;
using System.Collections.Generic;

namespace ConsoleApp
{
    class Program
    {        
        static void Main(string[] args)
        {
            string someString = @"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod 
                tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud 
                exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.";

            try
            {                
                var mostOftenFoundSymbols = GetMostOftenFoundSymbols(someString);

                Console.WriteLine(String.Join(", ", mostOftenFoundSymbols));   
            }
            catch(Exception exception)
            {
                Console.WriteLine($"Error: {exception.Message}");
            }
            finally
            {
                Console.ReadLine();
            }
        }
        
        public static List<char> GetMostOftenFoundSymbols( string exampleString )
        {
            if(String.IsNullOrEmpty(exampleString))
                throw new Exception("Given string is null or empty!");

            var symbolsCount = new Dictionary<char, int>();
            var selectedSymbols = new List<char>();
            var maxCount = 0;
 
            foreach(var character in exampleString)
            {
                if(!character.Equals(' '))
                {
                    if(!symbolsCount.ContainsKey(character))
                        symbolsCount[character] = 1;
                    else
                        symbolsCount[character] += 1;

                    if(symbolsCount[character] > maxCount) 
                    {
                        selectedSymbols = new List<char>() { character };
                        maxCount = symbolsCount[character];
                    }
                    else if(symbolsCount[character] == maxCount)
                        selectedSymbols.Add(character);
                }
            }

            return selectedSymbols;
        }
    }
}
